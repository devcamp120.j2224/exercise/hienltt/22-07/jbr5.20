package com.devcamp.artistalbum_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArtistAlbumApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArtistAlbumApiApplication.class, args);
	}

}
