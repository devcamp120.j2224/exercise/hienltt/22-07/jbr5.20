package com.devcamp.artistalbum_api.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.artistalbum_api.model.Artist;

@Service
public class ArtistService {
    
    public ArrayList<Artist> getListArtist() {
        AlbumService albumService = new AlbumService();
        
        ArrayList<Artist> listArtist = new ArrayList<>();
        Artist artist1 = new Artist(1, "Marry", albumService.getListAlbum1());
        Artist artist2 = new Artist(2, "John", albumService.getListAlbum2());
        Artist artist3 = new Artist(3, "David", albumService.getListAlbum3());
        listArtist.add(artist1);
        listArtist.add(artist2);
        listArtist.add(artist3);
        return listArtist;
    }
}
