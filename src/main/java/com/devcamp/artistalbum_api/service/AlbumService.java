package com.devcamp.artistalbum_api.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.artistalbum_api.model.Album;
@Service
public class AlbumService {

    Album album1 = new Album(1, "vol1", new ArrayList<String>());
    Album album2 = new Album(2, "vol2", new ArrayList<String>());
    Album album3 = new Album(3, "vol3", new ArrayList<String>());
    Album album4 = new Album(4, "vol4", new ArrayList<String>());
    Album album5 = new Album(5, "vol5", new ArrayList<String>());
    Album album6 = new Album(6, "vol6", new ArrayList<String>());
    Album album7 = new Album(7, "vol7", new ArrayList<String>());
    Album album8 = new Album(8, "vol8", new ArrayList<String>());
    Album album9 = new Album(9, "vol9", new ArrayList<String>());

    public ArrayList<Album> getListAlbum1(){
        ArrayList<Album> listAlbum1 = new ArrayList<>();
        listAlbum1.add(album1);
        listAlbum1.add(album2);
        listAlbum1.add(album3);
        return listAlbum1;
    }

    public ArrayList<Album> getListAlbum2(){
        ArrayList<Album> listAlbum2 = new ArrayList<>();
        listAlbum2.add(album4);
        listAlbum2.add(album5);
        listAlbum2.add(album6);
        return listAlbum2;
    }

    public ArrayList<Album> getListAlbum3(){
        ArrayList<Album> listAlbum3 = new ArrayList<>();
        listAlbum3.add(album7);
        listAlbum3.add(album8);
        listAlbum3.add(album9);
        return listAlbum3;
    }

    public ArrayList<Album> getListAllAlbum(){
        ArrayList<Album> listAllAlbum = new ArrayList<>();
        listAllAlbum.addAll(getListAlbum1());
        listAllAlbum.addAll(getListAlbum2());
        listAllAlbum.addAll(getListAlbum3());
        return listAllAlbum;
    }
}
