package com.devcamp.artistalbum_api.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.artistalbum_api.model.Artist;
import com.devcamp.artistalbum_api.service.ArtistService;

@RestController
public class ArtistController {
    @Autowired
    private ArtistService artistService;

    @CrossOrigin
    @GetMapping("/artists")
    public ArrayList<Artist> getListArtist(){        
        ArrayList<Artist> allArtist = artistService.getListArtist();
        return allArtist;
    }

    @CrossOrigin
    @GetMapping("/artists-info")
    public Artist requestArtist(@RequestParam(required = true, name = "artistId") int id) {
        ArrayList<Artist> allArtist = artistService.getListArtist();
        Artist artistFound = new Artist();
        for (Artist artist : allArtist) {
            if (artist.getId() == id) {
                artistFound = artist;
            }
        }
        return artistFound;
    }
}
