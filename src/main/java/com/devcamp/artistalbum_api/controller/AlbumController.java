package com.devcamp.artistalbum_api.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.artistalbum_api.model.Album;
import com.devcamp.artistalbum_api.service.AlbumService;

@RestController
public class AlbumController {
    @Autowired
    private AlbumService albumService;

    @CrossOrigin
    @GetMapping("/album-info")
    public Album requestAlbum(@RequestParam(required = true, name = "albumId") int id) {
        ArrayList<Album> listAlb = albumService.getListAllAlbum();
        Album albumFound = new Album();
        for (Album album : listAlb) {
            if (album.getId() == id) {
                albumFound = album;
            }
        }
        return albumFound;
    }
}
